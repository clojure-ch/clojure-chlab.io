(set-env!
 :source-paths #{"src"}
 :resource-paths #{"resources" "assets"}
 :dependencies '[[org.clojure/clojure "1.8.0"]
                 [perun "0.4.2-SNAPSHOT"]
                 [hiccup "1.0.5"]
                 [garden "1.3.2"]
                 [pandeiro/boot-http "0.7.3"]
                 [markdown-clj "1.0.1"]
                 [jeluard/boot-notify "0.1.2" :scope "test"]
                 [cpmcdaniel/boot-copy "1.0"]
                 [clj-http "2.3.0"]
                 [org.clojure/data.json "0.2.6"]
                 [deraen/boot-sass "0.3.1"]
                 [fsdb "0.1.1-SNAPSHOT"]
                 [camel-snake-kebab "0.4.0"]])

(require '[boot.core :as boot]
         '[io.perun :as perun]
         '[io.perun.meta :as meta]
         '[io.perun.core :as perun-core]
         ;;'[ch.clojure.data.index :as index-view]
         '[ch.clojure.post :as post-view]
         '[pandeiro.boot-http :refer [serve]]
         '[garden.core :refer [css]]
         '[cpmcdaniel.boot-copy :refer :all]
         '[clojure.data.json]
         '[clj-http.client]
         '[fsdb.core :as fsdb]
         '[deraen.boot-sass :refer [sass]])

(task-options!
 copy {:output-dir "target/public"
       ;; TODO: Make this regexp more readable. It has three parts:
       ;;       google search console + favicon stuff + other assets.
       :matching   #{#"(google.*\.html|safari-pinned-tab\.svg|favicon\.ico|browserconfig\.xml|manifest\.json)|\.(css|js|png|jpg|svg|gif)$"}})

(defn slug-fn
  "Slugs are derived from filenames of html files. They can have a
  YYYY-MM-DD- prefix or not."
  [_ {:keys [filename]}]
  (last (re-find #"(\d+-\d+-\d+-|)(.*)\.html" filename)))

(deftask set-meta-data
  "Add :key attribute with :val value to each file metadata and also
   to the global meta"
  [k key VAL kw "meta-data key"
   v val VAL str "meta-data value"]
  (with-pre-wrap fileset
    (let [files           (meta/get-meta fileset)
          global-meta     (meta/get-global-meta fileset)
          updated-files   (map #(assoc % key val) files)
          new-global-meta (assoc global-meta key val)
          updated-fs      (meta/set-meta fileset updated-files)]
      (meta/set-global-meta updated-fs new-global-meta))))

(deftask categories
  "Add :categories of all posts to the meta-data"
  []
  (with-pre-wrap fileset
    (let [files           (meta/get-meta fileset)
          global-meta     (meta/get-global-meta fileset)
          categories      (filter #(:category %) files)
          updated-files   (map #(assoc % :categories categories) files)
          new-global-meta (assoc global-meta :categories categories)
          updated-fs      (meta/set-meta fileset updated-files)]
      (meta/set-global-meta updated-fs new-global-meta))))

(defn category-grouper
  [entries]
  (reduce (fn [result entry]
            (let [category (:category entry)
                  path (str category ".html")]
              (-> result
                  (update-in [path :entries] conj entry)
                  (assoc-in [path :entry :category] category))))
          {}
          entries))

(deftask fsdb
  "Loads fsdb and adds it as meta to the fileset."
  [f fsdb FSDB str "fsdb path"]
  (with-pre-wrap fileset
    (->> (fsdb/read-tree "data")
         ;; have to use :io.perun.global here to have it merged into
         ;; static's meta
         (assoc-in (meta fileset) [:io.perun.global :fsdb])
         (with-meta fileset))))

(deftask print-global-meta
  []
  (with-pre-wrap fileset
    (perun-core/report-info "print-global-meta" (meta fileset))
    fileset))

(defn my-grouper [group-key global-meta entries]
  (let [items (-> global-meta :fsdb :data group-key)]
    ;; TODO `apply merge map`, feels like it should use `into`
    (apply merge (map (fn [[key item]]
                        {(str (name key) ".html")
                         {:entries []
                          :entry nil
                          :meta global-meta
                          :meta-entry item}}) items))))

(deftask global-assortment
  "Renders an assortment using global metadata"
  [o out-dir    OUTDIR     str   "the output directory"
   r renderer   RENDERER   sym   "page renderer (fully qualified symbol resolving to a function)"
   k group-key  GROUPKEY   kw    "key on which to group"]
  (fn [next-task]
    (fn [fileset]
      (let [global-meta (io.perun.meta/get-global-meta fileset)
            task-fn (perun/assortment-task {:task-name "global-assortment"
                                            :renderer renderer
                                            :out-dir out-dir
                                            :filterer identity
                                            ;;:extensions [".html"]
                                            :sortby :date-published
                                            :comparator (fn [i1 i2] (compare i2 i1))
                                            :tracer :your.ns/global-assortment
                                            :grouper (partial my-grouper group-key global-meta)})]
        ((task-fn next-task) fileset)))))

(deftask build
  "Build the site."
  []
  (let [is-of-type? (fn [{:keys [permalink]} doc-type] (.startsWith permalink (str "/" doc-type)))]
    (comp
     (perun/markdown)
     (perun/draft)
     ;;(print-meta)
     (perun/slug :slug-fn slug-fn)
     (perun/ttr)
     (categories)
     (perun/word-count)
     (perun/build-date)
     (perun/gravatar
      :source-key :author-email
      :target-key :author-gravatar)

     (perun/collection
      :renderer 'ch.clojure.index/render
      :page "blog.html"
      ;; Order pages in reverse chronological order
      :sortby #(:date-created %)
      :comparator #(.compareTo %2 %1)
      :filterer #(is-of-type? % "posts"))

     (perun/collection
      :renderer 'ch.clojure.project/render-collection
      :page "projects.html"
      :filterer #(is-of-type? % "projects"))

     (fsdb)
     ;;(print-global-meta)

     (perun/static :renderer 'ch.clojure.data.index/page        :page "index.html")

     ;;(global-assortment :renderer 'ch.clojure.data.technology/page
     ;;                   :out-dir "public/technology"
     ;;                   :group-key :technologies)

     ;; TODO omit for prod
     ;;(perun/static :renderer 'ch.clojure.data.debug/page        :page "debug.html")

     ;; Groups all posts that have a :category (yes, only a single one
     ;; atm) into one file. For example a post with a :category of
     ;; "emacs" will be rendered into a file
     ;; "public/category/emacs.html" together with every other post
     ;; with the same tag.
     (perun/assortment :renderer 'ch.clojure.index/render
                       :grouper category-grouper
                       :out-dir "public/category"
                       :filterer #(is-of-type? % "posts"))

     ;; renders each md file in posts into its own page
     (perun/render :renderer 'ch.clojure.post/render
                   :filterer #(is-of-type? % "posts"))

     (perun/sitemap)
     ;;(print-global-meta)
     (perun/atom-feed :filterer #(is-of-type? % "posts")
                      ;; setting base-url explicitly is a quickfix for
                      ;; a perun bug of some sort
                      :base-url "http://clojure.ch/")
     ;; TODO
     ;;(perun/static :renderer 'ch.clojure.static/letsencrypt
     ;;              :page ".well-known/acme-challenge/zN70yAoyS5KLi097MJd11yzKm0SKLGVDCU2lDAARsx0")

     ;;(notify)
     ;;(print-meta)
     (sass)
     (target)
     (copy))))

(deftask dev
  []
  (comp
   (watch)
   (perun/global-metadata)
   (set-meta-data :key :target
                  :val "dev")
   (build)
   (serve :dir "target/public")))

(deftask prod
  []
  (comp
   (perun/global-metadata)
   (set-meta-data :key :target
                  :val "prod")
   (build)))
