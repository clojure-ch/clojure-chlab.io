(ns ch.clojure.static
  (:use [hiccup.core :only (html)]
        [hiccup.page :only (html5)]))

(defn letsencrypt [{global-meta :meta pages :entries page :entry}]
  "zN70yAoyS5KLi097MJd11yzKm0SKLGVDCU2lDAARsx0.8bqBnIXMW-HZOyMwueHpbj4M2wSuM2LANX3xy9H02b0")
