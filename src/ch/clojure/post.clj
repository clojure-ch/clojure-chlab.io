(ns ch.clojure.post
  (:require [ch.clojure.index])
  (:use [hiccup.core :only (html)]
        [hiccup.page :only (html5)]))


(defn render [{global-meta :meta posts :entries post :entry}]
  (ch.clojure.index/layout global-meta posts
                   [:main.single-post
                    [:div#content
                     (ch.clojure.index/render-post post)]]))
