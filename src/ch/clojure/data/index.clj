(ns ch.clojure.data.index
  (:require [ch.clojure.layout :as layout]))

(defn company [c]
  [:li
   (:name c)
   ", "
   (:city c)
   ", "
   [:a {:href (:url c)} (:url c)]
   ])

(defn meetup [m]
  [:li [:a {:href (:url m)} (:name m)]])

(defn page [arg]
  (let [db (get-in arg [:meta :fsdb :data])
        companies (->> db :companies vals (sort-by :name))
        meetups (-> db :meetups vals)]
    (layout/main
     arg
     [:main.twocolumn
      [:div.companies
       [:h2 "Companies Using Clojure"]
       (into [:ul] (map company companies))
       [:div
        [:hr]
        [:h3 "Missing a company?"]
        "Please help us to improve this site, by creating a Merge Request on the "
        [:a {:href "https://gitlab.com/clojure-ch/clojure-ch.gitlab.io"} "git repository"]
        "."]]
      [:div.meetups
       [:h2 "Meetups for Clojure & Friends"]
       (into [:ul] (map meetup meetups))
       ;; TODO entries
       ]])))
