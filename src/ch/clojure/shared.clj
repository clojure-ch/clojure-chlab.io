(ns ch.clojure.shared
  (:use [markdown.core]
        [hiccup.core :only (html)]
        [hiccup.page :only (html5)]))

(defn description2paragraphs [text]
  ;; `md-to-html-string` would not pick up the newlines to create
  ;; paragraphs on it's own.
  (map (fn [text]
         (md-to-html-string text))
       (clojure.string/split text #"\n")))

(def dash " - ")

(def space " ")

(def comma ", ")

(def dot ".")

(defn render-footer []
  [:footer
   [:div {:itemscope true
          :itemprop "publisher"
          :itemtype "https://schema.org/Organization"}
    [:span.name {:itemprop "name"} "clojure.ch"]
    dash
    [:span
     "a site by"
     space
     "Phil Hofmann (branch14)"
     comma
     "Alain M. Lafon (munen)"
     space
     "&"
     space
     "Michael Locher (cmbntr)"
     dot]
    space
    [:div.contact
     "Drop us a line at"
     space
     [:a {:itemprop "email" :href "mailto:info@clojure.ch"} "info@clojure.ch"]]]])

(defn back-arrow []
  [:svg#back-arrow {:x "0px", :y "0px", :viewbox "0 0 30 30"}
   [:path {:d "M26.5,11.5H12.7l4.1-4.1c1.3-1.3,1.3-3.4,0-4.7c-1.3-1.3-3.4-1.3-4.7,0l-12,12c-0.2,0.2-0.2,0.5,0,0.7l12,12\n\tc0.6,0.6,1.5,1,2.4,1s1.7-0.3,2.4-1c1.3-1.3,1.3-3.4,0-4.7l-4.1-4.1h13.8c1.9,0,3.5-1.6,3.5-3.5S28.4,11.5,26.5,11.5z"}]])
